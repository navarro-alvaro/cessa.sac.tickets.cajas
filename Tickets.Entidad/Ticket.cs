﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tickets.Entidad
{
    public class Ticket
    {
        public Ticket() 
        {
            ticketEnUso.Tipo = string.Empty;
            ticketEnUso.Numero = 0;
        }

        //Estructura para almacenar la información
        private struct TTicket
        {
            public string Tipo;
            public int Numero;
            public string FechaSolicitud;
            public string HoraSolicitud;
            public string HoraAtencion;
        }

        TTicket ticketEnUso = new TTicket();

        //Propiedades
        public string Tipo
        {
            get { return ticketEnUso.Tipo; }
            set { ticketEnUso.Tipo = value; }
        }
        public int Numero
        {
            get { return ticketEnUso.Numero; }
            set { ticketEnUso.Numero = value; }
        }
        public string Tipo
        {
            get { return ticketEnUso.Tipo; }
            set { ticketEnUso.Tipo = value; }
        }
        public string FechaSolicitud
        {
            get { return ticketEnUso.FechaSolicitud; }
            set { ticketEnUso.FechaSolicitud = value; }
        }
        public string HoraSolicitud
        {
            get { return ticketEnUso.HoraSolicitud; }
            set { ticketEnUso.HoraSolicitud = value; }
        }
        public string HoraAtencion
        {
            get { return ticketEnUso.HoraAtencion; }
            set { ticketEnUso.HoraAtencion = value; }
        }
    }
}
