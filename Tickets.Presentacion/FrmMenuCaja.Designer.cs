﻿namespace Tickets.Presentacion
{
    partial class FrmMenuCaja
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.components = new System.ComponentModel.Container();
			this.TlpPrincipal = new System.Windows.Forms.TableLayoutPanel();
			this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
			this.TlpIzquierda = new System.Windows.Forms.TableLayoutPanel();
			this.LblBienvenido = new System.Windows.Forms.Label();
			this.PbxLogo = new System.Windows.Forms.PictureBox();
			this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
			this.BtnCaja = new System.Windows.Forms.Button();
			this.BtnCajaEmbarazadas = new System.Windows.Forms.Button();
			this.BtnCaja3raEdad = new System.Windows.Forms.Button();
			this.pictureBox2 = new System.Windows.Forms.PictureBox();
			this.pictureBox3 = new System.Windows.Forms.PictureBox();
			this.BtnCajaCapacidadesDistintas = new System.Windows.Forms.Button();
			this.pictureBox4 = new System.Windows.Forms.PictureBox();
			this.pictureBox1 = new System.Windows.Forms.PictureBox();
			this.LblCreditos = new System.Windows.Forms.Label();
			this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
			this.LblUltimoTicket = new System.Windows.Forms.Label();
			this.LblCaja = new System.Windows.Forms.Label();
			this.TmrPlataforma = new System.Windows.Forms.Timer(this.components);
			this.TlpPrincipal.SuspendLayout();
			this.tableLayoutPanel1.SuspendLayout();
			this.TlpIzquierda.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.PbxLogo)).BeginInit();
			this.tableLayoutPanel2.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
			this.tableLayoutPanel3.SuspendLayout();
			this.SuspendLayout();
			// 
			// TlpPrincipal
			// 
			this.TlpPrincipal.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.TlpPrincipal.BackColor = System.Drawing.Color.White;
			this.TlpPrincipal.ColumnCount = 1;
			this.TlpPrincipal.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.TlpPrincipal.Controls.Add(this.tableLayoutPanel1, 0, 0);
			this.TlpPrincipal.Controls.Add(this.LblCreditos, 0, 2);
			this.TlpPrincipal.Controls.Add(this.tableLayoutPanel3, 0, 1);
			this.TlpPrincipal.Location = new System.Drawing.Point(17, 17);
			this.TlpPrincipal.Margin = new System.Windows.Forms.Padding(10);
			this.TlpPrincipal.Name = "TlpPrincipal";
			this.TlpPrincipal.Padding = new System.Windows.Forms.Padding(5);
			this.TlpPrincipal.RowCount = 3;
			this.TlpPrincipal.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 65F));
			this.TlpPrincipal.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 35F));
			this.TlpPrincipal.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
			this.TlpPrincipal.Size = new System.Drawing.Size(808, 568);
			this.TlpPrincipal.TabIndex = 3;
			// 
			// tableLayoutPanel1
			// 
			this.tableLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.tableLayoutPanel1.ColumnCount = 2;
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tableLayoutPanel1.Controls.Add(this.TlpIzquierda, 0, 0);
			this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 1, 0);
			this.tableLayoutPanel1.Location = new System.Drawing.Point(8, 8);
			this.tableLayoutPanel1.Name = "tableLayoutPanel1";
			this.tableLayoutPanel1.RowCount = 1;
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 343F));
			this.tableLayoutPanel1.Size = new System.Drawing.Size(792, 343);
			this.tableLayoutPanel1.TabIndex = 3;
			// 
			// TlpIzquierda
			// 
			this.TlpIzquierda.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.TlpIzquierda.BackColor = System.Drawing.Color.White;
			this.TlpIzquierda.ColumnCount = 1;
			this.TlpIzquierda.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.TlpIzquierda.Controls.Add(this.LblBienvenido, 0, 0);
			this.TlpIzquierda.Controls.Add(this.PbxLogo, 0, 1);
			this.TlpIzquierda.Location = new System.Drawing.Point(3, 3);
			this.TlpIzquierda.Name = "TlpIzquierda";
			this.TlpIzquierda.Padding = new System.Windows.Forms.Padding(10);
			this.TlpIzquierda.RowCount = 2;
			this.TlpIzquierda.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 80F));
			this.TlpIzquierda.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.TlpIzquierda.Size = new System.Drawing.Size(390, 337);
			this.TlpIzquierda.TabIndex = 2;
			// 
			// LblBienvenido
			// 
			this.LblBienvenido.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.LblBienvenido.AutoSize = true;
			this.LblBienvenido.Font = new System.Drawing.Font("Tahoma", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.LblBienvenido.Location = new System.Drawing.Point(13, 10);
			this.LblBienvenido.Name = "LblBienvenido";
			this.LblBienvenido.Size = new System.Drawing.Size(364, 80);
			this.LblBienvenido.TabIndex = 2;
			this.LblBienvenido.Text = "Compañía Eléctrica Sucre S. A.";
			this.LblBienvenido.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// PbxLogo
			// 
			this.PbxLogo.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.PbxLogo.BackColor = System.Drawing.SystemColors.ControlLightLight;
			this.PbxLogo.ErrorImage = null;
			this.PbxLogo.Image = global::Tickets.Presentacion.Properties.Resources.logo_cessa;
			this.PbxLogo.InitialImage = null;
			this.PbxLogo.Location = new System.Drawing.Point(13, 93);
			this.PbxLogo.Name = "PbxLogo";
			this.PbxLogo.Size = new System.Drawing.Size(364, 231);
			this.PbxLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
			this.PbxLogo.TabIndex = 0;
			this.PbxLogo.TabStop = false;
			// 
			// tableLayoutPanel2
			// 
			this.tableLayoutPanel2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.tableLayoutPanel2.BackColor = System.Drawing.Color.DarkBlue;
			this.tableLayoutPanel2.ColumnCount = 2;
			this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 158F));
			this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tableLayoutPanel2.Controls.Add(this.BtnCaja, 1, 1);
			this.tableLayoutPanel2.Controls.Add(this.BtnCajaEmbarazadas, 1, 3);
			this.tableLayoutPanel2.Controls.Add(this.BtnCaja3raEdad, 1, 2);
			this.tableLayoutPanel2.Controls.Add(this.pictureBox2, 0, 2);
			this.tableLayoutPanel2.Controls.Add(this.pictureBox3, 0, 3);
			this.tableLayoutPanel2.Controls.Add(this.BtnCajaCapacidadesDistintas, 1, 4);
			this.tableLayoutPanel2.Controls.Add(this.pictureBox4, 0, 4);
			this.tableLayoutPanel2.Controls.Add(this.pictureBox1, 0, 1);
			this.tableLayoutPanel2.Location = new System.Drawing.Point(399, 3);
			this.tableLayoutPanel2.Name = "tableLayoutPanel2";
			this.tableLayoutPanel2.Padding = new System.Windows.Forms.Padding(20);
			this.tableLayoutPanel2.RowCount = 6;
			this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 12F));
			this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
			this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
			this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
			this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
			this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 10F));
			this.tableLayoutPanel2.Size = new System.Drawing.Size(390, 337);
			this.tableLayoutPanel2.TabIndex = 3;
			// 
			// BtnCaja
			// 
			this.BtnCaja.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.BtnCaja.Font = new System.Drawing.Font("Tahoma", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.BtnCaja.Location = new System.Drawing.Point(188, 42);
			this.BtnCaja.Margin = new System.Windows.Forms.Padding(10);
			this.BtnCaja.Name = "BtnCaja";
			this.BtnCaja.Size = new System.Drawing.Size(172, 48);
			this.BtnCaja.TabIndex = 6;
			this.BtnCaja.TabStop = false;
			this.BtnCaja.Text = "CAJA";
			this.BtnCaja.UseVisualStyleBackColor = true;
			this.BtnCaja.Click += new System.EventHandler(this.BtnCaja_Click);
			// 
			// BtnCajaEmbarazadas
			// 
			this.BtnCajaEmbarazadas.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.BtnCajaEmbarazadas.Font = new System.Drawing.Font("Tahoma", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.BtnCajaEmbarazadas.Location = new System.Drawing.Point(188, 178);
			this.BtnCajaEmbarazadas.Margin = new System.Windows.Forms.Padding(10);
			this.BtnCajaEmbarazadas.Name = "BtnCajaEmbarazadas";
			this.BtnCajaEmbarazadas.Size = new System.Drawing.Size(172, 48);
			this.BtnCajaEmbarazadas.TabIndex = 8;
			this.BtnCajaEmbarazadas.TabStop = false;
			this.BtnCajaEmbarazadas.Text = "CAJA EMBARAZADAS";
			this.BtnCajaEmbarazadas.UseVisualStyleBackColor = true;
			this.BtnCajaEmbarazadas.Click += new System.EventHandler(this.BtnCajaEmbarazadas_Click);
			// 
			// BtnCaja3raEdad
			// 
			this.BtnCaja3raEdad.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.BtnCaja3raEdad.Font = new System.Drawing.Font("Tahoma", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.BtnCaja3raEdad.Location = new System.Drawing.Point(188, 110);
			this.BtnCaja3raEdad.Margin = new System.Windows.Forms.Padding(10);
			this.BtnCaja3raEdad.Name = "BtnCaja3raEdad";
			this.BtnCaja3raEdad.Size = new System.Drawing.Size(172, 48);
			this.BtnCaja3raEdad.TabIndex = 10;
			this.BtnCaja3raEdad.TabStop = false;
			this.BtnCaja3raEdad.Text = "CAJA 3RA EDAD";
			this.BtnCaja3raEdad.UseVisualStyleBackColor = true;
			this.BtnCaja3raEdad.Click += new System.EventHandler(this.BtnCaja3raEdad_Click);
			// 
			// pictureBox2
			// 
			this.pictureBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.pictureBox2.Image = global::Tickets.Presentacion.Properties.Resources.persona_3ra_edad;
			this.pictureBox2.Location = new System.Drawing.Point(23, 103);
			this.pictureBox2.Name = "pictureBox2";
			this.pictureBox2.Size = new System.Drawing.Size(152, 62);
			this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
			this.pictureBox2.TabIndex = 12;
			this.pictureBox2.TabStop = false;
			// 
			// pictureBox3
			// 
			this.pictureBox3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.pictureBox3.Image = global::Tickets.Presentacion.Properties.Resources.persona_embarazada;
			this.pictureBox3.Location = new System.Drawing.Point(23, 171);
			this.pictureBox3.Name = "pictureBox3";
			this.pictureBox3.Size = new System.Drawing.Size(152, 62);
			this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
			this.pictureBox3.TabIndex = 13;
			this.pictureBox3.TabStop = false;
			// 
			// BtnCajaCapacidadesDistintas
			// 
			this.BtnCajaCapacidadesDistintas.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.BtnCajaCapacidadesDistintas.Font = new System.Drawing.Font("Tahoma", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.BtnCajaCapacidadesDistintas.Location = new System.Drawing.Point(188, 246);
			this.BtnCajaCapacidadesDistintas.Margin = new System.Windows.Forms.Padding(10);
			this.BtnCajaCapacidadesDistintas.Name = "BtnCajaCapacidadesDistintas";
			this.BtnCajaCapacidadesDistintas.Size = new System.Drawing.Size(172, 48);
			this.BtnCajaCapacidadesDistintas.TabIndex = 14;
			this.BtnCajaCapacidadesDistintas.TabStop = false;
			this.BtnCajaCapacidadesDistintas.Text = "CAJA EMBARAZADAS";
			this.BtnCajaCapacidadesDistintas.UseVisualStyleBackColor = true;
			this.BtnCajaCapacidadesDistintas.Click += new System.EventHandler(this.BtnCajaCapacidadesDistintas_Click);
			// 
			// pictureBox4
			// 
			this.pictureBox4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.pictureBox4.Image = global::Tickets.Presentacion.Properties.Resources.persona_capacidades_distintas;
			this.pictureBox4.Location = new System.Drawing.Point(23, 239);
			this.pictureBox4.Name = "pictureBox4";
			this.pictureBox4.Size = new System.Drawing.Size(152, 62);
			this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
			this.pictureBox4.TabIndex = 15;
			this.pictureBox4.TabStop = false;
			// 
			// pictureBox1
			// 
			this.pictureBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.pictureBox1.Image = global::Tickets.Presentacion.Properties.Resources.persona_normal;
			this.pictureBox1.Location = new System.Drawing.Point(23, 35);
			this.pictureBox1.Name = "pictureBox1";
			this.pictureBox1.Size = new System.Drawing.Size(152, 62);
			this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
			this.pictureBox1.TabIndex = 11;
			this.pictureBox1.TabStop = false;
			// 
			// LblCreditos
			// 
			this.LblCreditos.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.LblCreditos.AutoSize = true;
			this.LblCreditos.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.LblCreditos.Location = new System.Drawing.Point(8, 542);
			this.LblCreditos.Name = "LblCreditos";
			this.LblCreditos.Size = new System.Drawing.Size(792, 21);
			this.LblCreditos.TabIndex = 4;
			this.LblCreditos.Text = "Departamento de Tecnologías de Información y Comunicación - 2017 © Compañía Eléct" +
    "rica Sucre S. A.";
			this.LblCreditos.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// tableLayoutPanel3
			// 
			this.tableLayoutPanel3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.tableLayoutPanel3.BackColor = System.Drawing.Color.Black;
			this.tableLayoutPanel3.ColumnCount = 1;
			this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tableLayoutPanel3.Controls.Add(this.LblUltimoTicket, 0, 1);
			this.tableLayoutPanel3.Controls.Add(this.LblCaja, 0, 0);
			this.tableLayoutPanel3.Font = new System.Drawing.Font("Tahoma", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.tableLayoutPanel3.Location = new System.Drawing.Point(10, 359);
			this.tableLayoutPanel3.Margin = new System.Windows.Forms.Padding(5);
			this.tableLayoutPanel3.Name = "tableLayoutPanel3";
			this.tableLayoutPanel3.Padding = new System.Windows.Forms.Padding(20);
			this.tableLayoutPanel3.RowCount = 2;
			this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
			this.tableLayoutPanel3.Size = new System.Drawing.Size(788, 178);
			this.tableLayoutPanel3.TabIndex = 5;
			// 
			// LblUltimoTicket
			// 
			this.LblUltimoTicket.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.LblUltimoTicket.AutoSize = true;
			this.tableLayoutPanel3.SetColumnSpan(this.LblUltimoTicket, 2);
			this.LblUltimoTicket.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.LblUltimoTicket.ForeColor = System.Drawing.Color.Yellow;
			this.LblUltimoTicket.Location = new System.Drawing.Point(23, 138);
			this.LblUltimoTicket.Name = "LblUltimoTicket";
			this.LblUltimoTicket.Size = new System.Drawing.Size(742, 20);
			this.LblUltimoTicket.TabIndex = 11;
			this.LblUltimoTicket.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// LblCaja
			// 
			this.LblCaja.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.LblCaja.AutoSize = true;
			this.LblCaja.BackColor = System.Drawing.Color.Black;
			this.LblCaja.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.LblCaja.ForeColor = System.Drawing.Color.White;
			this.LblCaja.Location = new System.Drawing.Point(23, 20);
			this.LblCaja.Name = "LblCaja";
			this.LblCaja.Size = new System.Drawing.Size(742, 118);
			this.LblCaja.TabIndex = 4;
			this.LblCaja.Text = "Servicios brindados en CAJA:";
			// 
			// TmrPlataforma
			// 
			this.TmrPlataforma.Interval = 1000;
			this.TmrPlataforma.Tick += new System.EventHandler(this.TmrPlataforma_Tick);
			// 
			// FrmMenuCaja
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.LightSlateGray;
			this.ClientSize = new System.Drawing.Size(840, 600);
			this.Controls.Add(this.TlpPrincipal);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
			this.Name = "FrmMenuCaja";
			this.Padding = new System.Windows.Forms.Padding(5);
			this.Text = "FrmMenu";
			this.TopMost = true;
			this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
			this.TlpPrincipal.ResumeLayout(false);
			this.TlpPrincipal.PerformLayout();
			this.tableLayoutPanel1.ResumeLayout(false);
			this.TlpIzquierda.ResumeLayout(false);
			this.TlpIzquierda.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.PbxLogo)).EndInit();
			this.tableLayoutPanel2.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
			this.tableLayoutPanel3.ResumeLayout(false);
			this.tableLayoutPanel3.PerformLayout();
			this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel TlpPrincipal;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.Label LblCaja;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel TlpIzquierda;
        private System.Windows.Forms.Label LblBienvenido;
        private System.Windows.Forms.PictureBox PbxLogo;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Button BtnCaja;
        private System.Windows.Forms.Button BtnCajaEmbarazadas;
        private System.Windows.Forms.Label LblCreditos;
        private System.Windows.Forms.Button BtnCaja3raEdad;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.Timer TmrPlataforma;
        private System.Windows.Forms.Button BtnCajaCapacidadesDistintas;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.Label LblUltimoTicket;

    }
}