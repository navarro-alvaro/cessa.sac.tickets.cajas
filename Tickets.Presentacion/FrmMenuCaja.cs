﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Tickets.Presentacion
{
    public partial class FrmMenuCaja : Form
    {
        Negocio.Ticket ticketNegocio;
        RptTicket Reporte;
        //private Label ultimoTicket;

        int tiempoPantalla;
        FrmDialog dialogo;

        public FrmMenuCaja()
        {
            InitializeComponent();

            //Cursor.Hide();
            BtnCaja3raEdad.Text = "CAJA 3RA EDAD";
            BtnCajaEmbarazadas.Text = "CAJA EMBARAZADAS";
            BtnCajaCapacidadesDistintas.Text = "CAJA CAPACIDADES\nDISTINTAS";
            LblCaja.Text = "Servicios brindados en CAJA:\n"
                + "\n- Pago de Consumos"
                + "\n- Pago de Dividendos a Accionistas"
                + "\n- Otros referidos a Atención al Cliente";

            LblCreditos.Text = "Departamento de Tecnologías de Información y Comunicación - " + DateTime.Today.Year + " © Compañía Eléctrica Sucre S. A.";

            //ultimoTicket.Text = new ;
            ticketNegocio = new Negocio.Ticket();
            ticketNegocio.Errores += new Negocio.Ticket.ErroresEventHandler(ticket_Errores);
            ticketNegocio.MostrarInicio += new Negocio.Ticket.MostrarInicioEventHandler(ticket_MostrarInicio);

            TmrPlataforma.Enabled = true;
            tiempoPantalla = 0;
            dialogo = new FrmDialog();

            Reporte = new RptTicket();
        }

        void ticket_MostrarInicio()
        {
            LblUltimoTicket.Text = "El último Ticket generado fue el " + ticketNegocio.Tipo + "-" + ticketNegocio.Numero.ToString().PadLeft(3, '0');

            Reporte.Imprimir(
                ticketNegocio.Tipo + "-" + ticketNegocio.Numero.ToString().PadLeft(3, '0'),
                ticketNegocio.FechaSolicitud + " " + ticketNegocio.HoraSolicitud
            );

			//this.Close();
			PbxLogo.Focus();
        }

        void ticket_Errores(string mensaje)
        {
            MessageBox.Show(mensaje, "Ocurrió un error");
        }

        private void TmrPlataforma_Tick(object sender, EventArgs e)
        {
            //tiempoPantalla++;
            //if (tiempoPantalla == 5)
            //{
            //    dialogo.Close();
            //    botones(true);
            //    //TmrPlataforma.Enabled = false;
            //}
        }

		private void BtnCaja_Click(object sender, EventArgs e)
		{
			ticketNegocio.Tipo = "G";
            botones(false);
			ticketNegocio.Generar();
		}

		private void BtnCaja3raEdad_Click(object sender, EventArgs e)
		{
			ticketNegocio.Tipo = "H";
            botones(false);
			ticketNegocio.Generar();
		}

		private void BtnCajaEmbarazadas_Click(object sender, EventArgs e)
		{
			ticketNegocio.Tipo = "I";
            botones(false);
			ticketNegocio.Generar();
		}

		private void BtnCajaCapacidadesDistintas_Click(object sender, EventArgs e)
		{
			ticketNegocio.Tipo = "H";
            botones(false);
			ticketNegocio.Generar();
		}

        private void botones(bool activar)
        {
            //BtnCaja.Enabled = activar;
            //BtnCaja3raEdad.Enabled = activar;
            //BtnCajaCapacidadesDistintas.Enabled = activar;
            //BtnCajaEmbarazadas.Enabled = activar;
            //if (activar == false)
            //{
            //    tiempoPantalla = 0;
            //    dialogo.ShowDialog();
            //    dialogo.TopMost = true;
            //}
        }
	}
}
